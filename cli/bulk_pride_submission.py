import time, datetime
import json
import os
import re
import csv
import logging
import warnings
import hashlib
import urllib.request
import gzip
import zipfile
import xml.etree.ElementTree as ET
from collections import defaultdict
from itertools import chain
import pkg_resources
import io
import operator
from functools import reduce
from ftplib import FTP
from typing import List, Dict, Set, Any, Optional, Tuple, Union

import click
import pronto
from prompt_toolkit import prompt
from prompt_toolkit.validation import Validator,ValidationError
from prompt_toolkit.formatted_text import HTML as prompt_html
from prompt_toolkit.completion import WordCompleter
from prompt_toolkit.shortcuts import ProgressBar
import getpass


class objectview(object):
    def __init__(self, d):
        self.__dict__ = d


class CvValidator(Validator):
    # TODO init with pronto obo
    def validate(self, document):
        text = document.text
        if text and not text in self.terms:
            # TODO some prefix matching would be nice here
            raise ValidationError(message='This input contains no recognisable cv term', cursor_position=0)

    def __init__(self,terms: Set[str]):
        self.terms=terms
        super(Validator, self).__init__()


def sha256fromfile(filename: str) -> str:
    sha  = hashlib.sha256()
    b  = bytearray(128*1024)
    mv = memoryview(b)
    with open(filename, 'rb', buffering=0) as f:
        for n in iter(lambda : f.readinto(mv), 0):
            sha.update(mv[:n])
    return sha.hexdigest()

def cvTomzTabStr(cv: dict) -> Union[str,None]:
    if type(cv) == str:
        return str(cv)
    if any([x not in cv.keys() for x in ['accession', 'name', 'ref']]):
        logging.error("There was an incomplete cv param: {}".format(str(cv))) 
        return None 
    if 'value' in cv.keys():
        return "[{},{},{},{},]".format(cv['ref'],cv['accession'],cv['name'],str(cv['value']))  # trailing empty space for trailing comma
    else: 
        return "[{},{},{},]".format(cv['ref'],cv['accession'],cv['name'])  # trailing empty space for trailing comma

def match_files(filepathlist) -> Tuple[list,list]:
    """
    Does two things, matching the basenames (excl. the suffix) of given paths and assign file types to the matches.
    Returned are match items that have a type and the unassociated files.
    """

    match_by_basename = defaultdict(list)
    # struct will have a list of paths per key == basename (w/o file type suffix) - these are the implicit matches
    for path in filepathlist:
        bn = os.path.basename(path)
        # file_name, file_end = os.path.splitext(bn)
        match_name, _ = bn.split('.',1)
        # if file_end == ".gz" or file_end == ".zip":
        #     file_name, file_end_end = os.path.splitext(file_name)
        #     file_end = file_end_end + file_end
        # TODO this obviously will struggle with a pattern like common.ok.extension
        # match_by_basename[file_name].append(path)
        match_by_basename[match_name].append(path)

    unassociated = list(match_by_basename.values())  # list of lists - first all are unassociated
    match_idx = [i for i,v in enumerate(list(match_by_basename.values())) \
        if len(v)>1 or \
            (len(v)>0 \
                and any([vi.endswith("mzid") \
                    or vi.endswith("mzid.gz") for vi in v]))]
    match_idx.sort(reverse=True)
    matches = [unassociated.pop(i) for i in match_idx]  # list of lists 
    unassociated = list(chain.from_iterable(unassociated))  # flat list

    # matches = [['path1','path2'],['path3','path4','path5'],['wonkypath'],['path1','path3']]
    regexs = {'raw_path': re.compile(r'\.raw', re.IGNORECASE),
            'id_path': re.compile(r'\.mzid', re.IGNORECASE),
            'mzml_path': re.compile(r'\.mzML', re.IGNORECASE),}
    associated_matches = list()

    for match in matches:
        # associate with types
        types = dict()
        for t,r in regexs.items():
            types[t] = list(filter(r.search, match))
        typed = set()
        for m in types.values():
            typed.update(set(m))
        # check ambiguous
        topop = list()
        for tpe,assoc in types.items():
            if len(assoc)>1:
               logging.error("There was an ambiguous match in the project's files: {}".format(str(assoc))) 
               types[tpe] = None
            elif len(assoc)<1:
                topop.append(tpe)
            else: 
                types[tpe] = next(x for x in assoc)
        # get rid of default empties
        for x in topop:
            types.pop(x)
        # others may be ambiguous 
        if set(match)-typed:
            types['other_path'] = set(match)-typed

        # {'raw_path': '.test/JD_06232014_sample1_A.raw', 'id_path': '.test/JD_06232014_sample1_A.mzid.gz', 'other_path': {'.test/JD_06232014_sample1_A.jk', '.test/JD_06232014_sample1_A.funny'}, 'mzml_path': '.test/JD_06232014_sample1_A.ok.mzML'}
        # TODO switch to partial if raw files are missing
        if 'id_path' in types and 'mzml_path' in types and 'raw_path' in types:  # proceed 
            # add pxf entries
            associated_matches.append(types)
        else:
            if 'id_path' in types:
                peaksource = extract_peaksource(types['id_path'])  # TODO extract_peaksource with raw file possibility? raw and mzid is not enough for complete submission?
                peaksourcepath = None
                ambiguous_peaksource = False
                for fs in filepathlist:
                    if peaksource and fs.endswith(peaksource) and not peaksourcepath:
                        types['mzml_path'] = fs
                        peaksourcepath = fs
                        unassociated.remove(fs)
                    elif peaksource and fs.endswith(peaksource):
                        logging.error("There was an ambiguous match in the project's files: {}".format(str(s))) 
                        ambiguous_peaksource = True
                        break
                if not ambiguous_peaksource and 'mzml_path' in types and 'raw_path' in types:
                    associated_matches.append(types)
                else:
                    # revert back types - insufficient
                    unassociated.extend([x for x in types.values() if type(x)!=set])
                    for x in types.values():
                        if type(x) == set:
                            unassociated.extend(list(x))
            else:
                # revert back types - insufficient   
                unassociated.extend([x for x in types.values() if type(x)!=set])
                for x in types.values():
                    if type(x) == set:
                        unassociated.extend(list(x))

    return associated_matches, unassociated

def extract_peaksource( id_filename: str) -> Union[str,None]:
    namespaces = { 'mzml': 'http://psi.hupo.org/ms/mzml', "mzid1.1":"http://psidev.info/psi/pi/mzIdentML/1.1"}
    # TODO sniff version ox xml dialect
    if id_filename.endswith('.mzid') or id_filename.endswith('.mzid.gz'):
        spdt = '{}SpectraData'.format("{" + namespaces['mzid1.1'] + "}")
        inpt = '{}Inputs'.format("{" + namespaces['mzid1.1'] + "}")
        no_clear = False
        source = None
        with gzip.open(id_filename, 'rb') if id_filename.endswith('.mzid.gz') else open(id_filename, 'rb') as f:
            for event, elem in ET.iterparse(f, events=('start','end')):
                if event == "start":
                    if elem.tag == spdt:
                        no_clear = True
                if event == "end":
                    if elem.tag == spdt:
                        mzml_cv = False
                        for chil in elem.getchildren():
                            if chil.tag == "{}FileFormat".format('{'+namespaces['mzid1.1']+'}'):
                                for granchil in chil.getchildren():
                                    if granchil.tag == "{}cvParam".format('{'+namespaces['mzid1.1']+'}') \
                                            and granchil.attrib['accession']=="MS:1000584":
                                        mzml_cv = True
                        if mzml_cv:
                            source = elem.attrib['location']
                            source = os.path.basename(source)
                        elem.clear()
                        no_clear = False
                    if not no_clear:
                        elem.clear()
                    if elem.tag == inpt:
                        break
        return source
    else:
        logging.warning("Can't extract metadata from non-mzid file {}".format(id_filename) )
        return None

def extract_meta(mzml_filename: str, id_filename: str) -> Tuple[Union[List[Dict[str,str]],None],Union[List[Dict[str,str]],None]]:
    namespaces = { 'mzml': 'http://psi.hupo.org/ms/mzml', "mzid1.1":"http://psidev.info/psi/pi/mzIdentML/1.1"}
    # TODO sniff version ox xml dialect
    if not mzml_filename.endswith('.mzML'):
        logging.warning("Can't extract metadata from non-mzml file {}".format(mzml_filename) )
        instr = None
    else:
        instr = list()
        psims_gz = pkg_resources.resource_stream("cv_library", "psi-ms.obo.gz")
        with gzip.open(psims_gz, 'rb') as gzf:
            psims = pronto.Ontology(gzf,0)  # psi-ms imports seem to make pronto urllib.urlopen , so import depth = 0 

        i_cvs = {term.id for term in psims["MS:1000031"].subclasses()}      
        inta = '{}instrumentConfiguration'.format("{" + namespaces['mzml'] + "}")
        no_clear = False
        for event, elem in ET.iterparse(mzml_filename, events=('start','end')):
            if event == "start":
                if elem.tag == inta:
                    no_clear = True
            if event == "end":
                if elem.tag == inta:
                    for chil in elem.getchildren():
                        if chil.tag == "{}cvParam".format('{'+namespaces['mzml']+'}'):
                            if chil.attrib['accession'] in i_cvs:
                                instr.append({'accession': chil.attrib['accession'], 'name':chil.attrib['name'], 'ref': chil.attrib['cvRef']}) 
                    elem.clear()
                    no_clear = False
                    break
                if not no_clear:
                    elem.clear()
        
        # This is a fix for ThermoRawFileParser v1.1.11 generated files - and instrument type cvparam location is highly suspicious to be there invalid 
        if not instr:
            inta = '{}referenceableParamGroup'.format("{" + namespaces['mzml'] + "}")
            no_clear = False
            for event, elem in ET.iterparse(mzml_filename, events=('start','end')):
                if event == "start":
                    if elem.tag == inta:
                        no_clear = True
                if event == "end":
                    if elem.tag == inta:
                        for chil in elem.getchildren():
                            if chil.tag == "{}cvParam".format('{'+namespaces['mzml']+'}'):
                                if chil.attrib['accession'] in i_cvs:
                                    instr.append({'accession': chil.attrib['accession'], 'name':chil.attrib['name'], 'ref': chil.attrib['cvRef']}) 
                        elem.clear()
                        no_clear = False
                        break
                    if not no_clear:
                        elem.clear()

    if id_filename.endswith('.mzid') or id_filename.endswith('.mzid.gz'):
        mods = list()
        spta = '{}SearchModification'.format("{" + namespaces['mzid1.1'] + "}")
        mota = '{}ModificationParams'.format("{" + namespaces['mzid1.1'] + "}")
        no_clear = False
        
        with gzip.open(id_filename, 'rb') if id_filename.endswith('.mzid.gz') else open(id_filename, 'rb') as f:
            for event, elem in ET.iterparse(f, events=('start','end')):
                if event == "start":
                    if elem.tag == spta:
                        no_clear = True
                if event == "end":
                    if elem.tag == spta:
                        for chil in elem.getchildren():
                            if chil.tag == "{}cvParam".format('{'+namespaces['mzid1.1']+'}'):
                                mods.append({'accession': chil.attrib['accession'], 'name':chil.attrib['name'], 'ref': chil.attrib['cvRef']}) 
                        elem.clear()
                        no_clear = False
                    if not no_clear:
                        elem.clear()
                    if elem.tag == mota:
                        break
    else:
        logging.warning("Can't extract metadata from non-mzid file {}".format(id_filename) )
        mods = None

    # good resource on speeds http://blog.behnel.de/posts/faster-xml-stream-processing-in-python.html
    return instr, mods             

def bottom_toolbar(reminder: str) -> str:
    return prompt_html('This is <b><style bg="ansired">{}</style></b>!'.format(reminder))

case_sensitive_bar = bottom_toolbar("case sensitive")

def inquire_experiment(target: str, bottombar=case_sensitive_bar):
    # prd_url = "https://github.com/PRIDE-Utilities/pride-ontology/raw/master/pride_cv.obo"
    # with urllib.request.urlopen(prd_url, timeout=10) as prd_in:
    #     prd = pronto.Ontology(prd_in)
    # Invalid .obo, swithcing to locally fixed
    pride_cv_gz = pkg_resources.resource_stream("cv_library", "pride_cv.obo.gz")
    with gzip.open(pride_cv_gz, 'rb') as gzf:
        prd = pronto.Ontology(gzf)
    prd_completer = WordCompleter(list({term.name for term in prd["PRIDE:0000457"].subclasses() if term.name}))  # experiment type subclass
    etx = prompt("Enter experiment type: ", bottom_toolbar=bottombar, completer=prd_completer, validator=CvValidator({term.name for term in prd["PRIDE:0000457"].subclasses() if term.name}) )
    eid = next((term.id for term in prd["PRIDE:0000457"].subclasses() if term.name == etx), "default value")  # TODO not safe for Eve user  ,  should be checcked in prompt with CvValidator
    return {'ref':'PRIDE','accession':eid,'name':etx}
    
def inquire_organism(target: str, bottombar=case_sensitive_bar):
    # ncit_url = "http://purl.obolibrary.org/obo/ncit.owl"# TODO this one is an excessive 500Mb! way around? subfilter via NCIT_C14250 (Organism) .subclasses()
    # with urllib.request.urlopen(ncit_url, timeout=10) as ncit_in:
    #     ncit = pronto.Ontology(ncit_in)
    # Doesn't really work, switching to csv from submission tool
    species_gz = pkg_resources.resource_stream("cv_library", "pride_species.cv.gz")
    with gzip.open(species_gz, 'r') as gzf:
        species = {line.decode().strip().split('\t')[2]: line.decode().strip().split('\t')[1] for line in gzf.readlines()}
    species_completer = WordCompleter(list(species.keys()))  # experiment type subclass
    stx = prompt("Enter species type: ", completer=species_completer, bottom_toolbar=bottombar, validator=CvValidator(set(species.keys())) )
    return {'ref':'NEWT','accession':species[stx],'name':stx}

def inquire_tissue(target: str, bottombar=case_sensitive_bar):
    # bto_url = "https://www.ebi.ac.uk/ols/ontologies/bto/download"
    # with urllib.request.urlopen(bto_url, timeout=10) as bto_in:
    #     bto = pronto.Ontology(bto_in)
    with warnings.catch_warnings(record=True) as w:
        tissue_gz = pkg_resources.resource_stream("cv_library", "bto.owl.gz")
        with gzip.open(tissue_gz, 'r') as gzf:
            bto = pronto.Ontology(gzf)

    bto_completer = WordCompleter([bto[term].name for term in bto if bto[term].name])
    ttx = prompt('Enter tissue type: ', bottom_toolbar=bottombar, completer=bto_completer, validator=CvValidator({bto[term].name for term in bto if bto[term].name}))
    tid = next((bto[term_k].id for term_k in bto if term_k and bto[term_k].name == ttx), "default value")  # TODO not safe for Eve user  ,  should be checcked in prompt with CvValidator
    return {'ref':'BTO','accession':tid,'name':ttx}

def filter_submission_metadata(unassociated):
    submission_metadata_update = list()
    filtered_unassociated = list()
    for it in unassociated:
        submission_metadata_update.append(it) \
            if it.endswith(".json") and "metadata" in it \
            else filtered_unassociated.append(it)
    return submission_metadata_update, filtered_unassociated 

@click.command()  # no command necessary if it's the only one 
@click.pass_context
@click.option('--prepared', type=click.Path(), default=None, help="The absolute path of the file with prepared submission data.")
@click.option('--folder', multiple=True, required=True, type=click.Path(), default=None, help="The absolute path of the folder(s) to submit. Inside each, there should be the files to be submitted directly, no subfolders.")
def start(ctx, prepared, folder):
    if prepared:
        try:
            with open(prepared, "r") as p:
                prep = json.load(p)
            click.echo("A prepared submission settings is used, will skip inquiry for all contained settings!")
        except:
            prep = dict()
            click.echo("No prepared submission settings readable, will overwrite!")
    else:
        prep = dict()

    # required files prompt
    click.echo('You will need your sample processing protocol available in a file named `sample_processing_protocol.md`!')
    click.echo('You will need your data processing protocol available in a file named `data_processing_protocol.md`!')
    click.echo('You will be prompted for a number of informations about the submission.')
    if click.confirm('Ready?'):
        click.echo('Here we go!')
        while not os.path.isfile('sample_processing_protocol.md') or not os.path.isfile('data_processing_protocol.md'):
            click.echo('Nope, nothing!')
            click.confirm('Now?')

    # simple names and titles prompts
    prompts = dict()
    if not all([x in prep for x in ["submitter_name","submitter_email","submitter_affiliation",\
                            "submitter_pride_login","lab_head_name","lab_head_email",\
                            "lab_head_affiliation","project_title","project_description"]]):
        kok = False
        while not kok:
            pls = "Please enter your {}"
            prompts = {
                "submitter_name": pls.format("name"),
                "submitter_email": pls.format("email"),
                "submitter_affiliation": pls.format("affiliation (i.e. institution)"),
                "submitter_pride_login": pls.format("username for pride login"),
                "lab_head_name": pls.format("lab head's name"),
                "lab_head_email": pls.format("lab head's email"),
                "lab_head_affiliation": pls.format("lab head's affiliation (i.e. institution)"),
                "project_title": pls.format("a project title"),
                "project_description": pls.format("a concise project description")
            }
            for item, prompt_text in prompts.items():
                prompts[item] = click.prompt(prompt_text)
                if item == "project_title":
                    while len(prompts[item])<30:
                        click.echo('Title must be over 30 characters long.')
                        prompts[item] = click.prompt(prompt_text)
            kok = click.confirm('Ok?')
        prep.update(prompts)

    # required files use
    if not all([x in prep for x in ["sample_processing_protocol","data_processing_protocol"]]):
        try:
            with open('sample_processing_protocol.md', 'r') as mdfile:
                mdft = mdfile.read()
                prompts["sample_processing_protocol"] = mdft.strip()
            with open('data_processing_protocol.md', 'r') as mdfile:
                mdft = mdfile.read()
                prompts["data_processing_protocol"] = mdft.strip()
        except:
            click.echo(':( no *_processing_protocol.md files found. Abort!')
        prep.update(prompts)

    # keywords prompt
    if not all([x in prep for x in ["keywords"]]):
        kok = False
        while not kok:
            keywords = click.prompt("Keywords (comma sparated, finalised by enter)")
            prompts['keywords'] = [k.strip() for k in keywords.split(',')]
            click.echo('Your keywords ' + ','.join(prompts['keywords']))
            kok = click.confirm('Ok?') 
        prep.update(prompts)

    # tissue and organism prompts
    case_sensitive_toolbar = bottom_toolbar("case sensitive")
    if not "experiment_type" in prep:
        prompts["experiment_type"] = cvTomzTabStr(inquire_experiment("ALL SAMPLES"))
        prep.update(prompts)
    if not all([x in prep for x in ['species', 'tissue']]):
        if click.confirm('Are all your samples of one organism and one tissue type?'):
            prompts['species'] = cvTomzTabStr(inquire_organism("ALL SAMPLES"))
            prompts['tissue'] = cvTomzTabStr(inquire_tissue("ALL SAMPLES"))
        else:
            click.echo("You will have to enter each samples organism and tissue type separately!")
        prep.update(prompts)

    prompts = prep

    # mods and instr prompts
    instruments = list()
    modifications = list()
    matches = list()
    associated_matches = list()
    submission_metadata = list()
    click.echo("Collecting metadata from files within the given folders.")
    for fold in folder:
        # get files, filter away hidden, 
        filtered = [os.path.abspath(os.path.join(fold,x)) for x in os.listdir(fold) if not x.startswith('.') and os.path.isfile(os.path.join(fold,x))]  # TODO make sure the folders are abs_paths
        ign = [os.path.join(fold,x) for x in os.listdir(fold) if x.startswith('.')]
        igd = [os.path.join(fold,x) for x in os.listdir(fold) if os.path.isdir(os.path.join(fold,x))]
        # some communication
        if ign:
            logging.warning("Ignoring these files: {}".format(','.join(ign)))
        if igd:
            igd_list = list(set(igd) - {os.path.normpath(f) for f in folder})
            logging.warning("Ignoring these directories: {}".format(','.join(igd_list)))
        filtered = {f: sha256fromfile(f) for f in filtered}  # TODO redefining from list to dict probably not that nice

        # matches per folder
        associated_matches_update, unassociated = match_files(filtered)
        
        # filter metadata to be submitted then communicate files not to be submitted
        submission_metadata_update = list()
        if unassociated:
            submission_metadata_update, unassociated = filter_submission_metadata(unassociated)
            submission_metadata.extend(submission_metadata_update)
            if unassociated:
                logging.warning("Ignoring these unmatched files: {}".format(','.join(unassociated)))

        # match metadata
        for match in associated_matches_update:
            match_instruments, match_modifications = extract_meta(match['mzml_path'], match['id_path'])
            if match_instruments:
                instruments += match_instruments
            if match_modifications:
                modifications += match_modifications
            
            match['mods'] = match_modifications
            match['instruments'] = match_instruments
            if 'species' in prompts and 'tissue' in prompts:
                match['species'] = prompts['species']
                match['tissue'] = prompts['tissue']
            else:
                match['species'] = inquire_organism(match['mzml_path'])
                match['tissue'] = inquire_tis(match['mzml_path'])
        # update associated_matches with update
        associated_matches.extend(associated_matches_update)
        # TODO check for clashes

    # write hash buffer
    hashfilename = "sha256.sums"
    hashfilebuffer = io.StringIO()
    hashfilebuffer.write( '\n'.join(['{}:{}'.format(k,v) for k,v in filtered.items()]) )
    print(submission_metadata)
    submission_metadata = {f: sha256fromfile(f) for f in submission_metadata}  
    hashfilebuffer.write( '\n'.join(['{}:{}'.format(k,v) for k,v in submission_metadata.items()]) )

    # assign `uid`
    count = 1  # doc 'suggests 123'
    for match in associated_matches:
        for x in ['id_', 'mzml_', 'raw_']:
            if x+'path' in match:
                match[x+'id'] = count
                count += 1
        others_count = len(match['other_path']) if 'other_path' in match else 0
        match['other_id'] = list(range(count,count+others_count))
        count = count+others_count
    for k in submission_metadata.keys():
        submission_metadata[k] = count
        count += 1

    # adjust prompts 
    if type(prompts['keywords']) == list:
        prompts['keywords'] = ', '.join(prompts['keywords'])     

    # write px file
    submissionfilename = "submission.px"
    submissionfilebuffer = io.StringIO()
    tsv_writer = csv.writer(submissionfilebuffer, delimiter='\t')

    tsv_writer.writerows([['MTD',k,v] for k,v in prompts.items()]) # no header
    # TODO exclude organism, tissue, etc?
    tsv_writer.writerows([['MTD', 'instrument', v] for v in {cvTomzTabStr(x) for x in instruments}])
    tsv_writer.writerows([['MTD', 'modification', v] for v in {cvTomzTabStr(x) for x in modifications}])
    tsv_writer.writerow(['MTD', 'submission_type', 'COMPLETE'])
    tsv_writer.writerow("")  # empty row

    sml = list()
    fm = ['FMH', 'file_id', 'file_type', 'file_path', 'file_mapping']
    tsv_writer.writerow(fm)  # header
    for match in associated_matches:
        for x,tx in zip(['id_', 'mzml_', 'raw_'],['RESULT','PEAK','RAW']):
            if tx == 'RAW' and x+'path' not in match:
                continue
            
            row_list = ['FME', match[x+'id'], tx, match[x+'path']]
            if tx == 'RAW' and match[x+'path'].endswith(".zip"):
                row_list[-1] = os.path.splitext(row_list[-1])[0]
            if tx == 'RESULT':
                assoc_ids = list()
                for tuid in ['mzml_id','raw_id']:
                    if tuid in match:
                        assoc_ids.append(str(match[tuid]))
                if 'other_id' in match:
                    assoc_ids.extend([str(i) for i in match['other_id']])

                row_list.append( ','.join(assoc_ids) )
                 # SMH     file_id species tissue  cell_type       disease modification    instrument      quantification  experimental_factor
                sml.append([
                    'SME', str(match['id_id']), str(match['species']), 
                    str(match['tissue']), '', '', 
                    ','.join([cvTomzTabStr(v) for v in match['mods']]), 
                    cvTomzTabStr(next(iter(match['instruments'][0:]), "Not provided")), 
                    '', "Not provided"])
            tsv_writer.writerow(row_list)
        if 'other_path' in match:
            for oid, opath in zip(match['other_id'],match['other_path']):
                tsv_writer.writerow(['FME', oid, 'OTHER', opath])

    for mpath,mid in submission_metadata.items():
        tsv_writer.writerow(['FME', mid, 'OTHER', mpath])

    tsv_writer.writerow("")  # empty row

    sm = ['SMH', 'file_id', 'species', 'tissue', 'cell_type', 'disease', 'modification', 'instrument', 'quantification', 'experimental_factor']
    tsv_writer.writerow(sm)  # header
    tsv_writer.writerows(sml)
    tsv_writer.writerow("")  # empty row

    print("Submission manifest:")
    print(submissionfilebuffer.getvalue())
    # with open('/tmp/debug.px', 'w') as px:
    #     px.write(submissionfilebuffer.getvalue())

    #write prepared file
    if prepared:
        with open(prepared, 'w') as p:
            json.dump(prep,p)
    
    with FTP('ftp-private.ebi.ac.uk') as ftp:
        kok = False
        while not kok:
            password = prompt('Enter provided ftp password: ', is_password=True)
            ftpfolder = prompt('Enter provided ftp folder name: ')
            try:
                ftp.login(user='pride-drop-006', passwd=password)
                ftp.cwd(ftpfolder)
                kok = True
            except: 
                click.echo("FTP folder or pass incorrect, sorry")
        
        hashfilebuffer.seek(0)
        resp = ftp.storbinary("STOR {}".format(hashfilename), io.BytesIO(hashfilebuffer.getvalue().encode()))
        submissionfilebuffer.seek(0)
        resp = ftp.storbinary("STOR {}".format(submissionfilename), io.BytesIO(submissionfilebuffer.getvalue().encode()))

        # TODO multiple upload connections - issue there: https://stackoverflow.com/questions/15053602/multiprocessing-ftp-uploading-with-a-precise-number-of-connections
        up_files = dict()
        for match in associated_matches:
            for x,tx in zip(['id_', 'mzml_', 'raw_'],['RESULT','PEAK','RAW']):
                if tx == 'RAW' and x+'path' not in match:
                    continue
                else:
                    fn = os.path.basename(match[x+'path'])
                    opath = match[x+'path']
                    up_files[fn] = opath
            if 'other_path' in match:
                for oid, opath in zip(match['other_id'],match['other_path']):
                        fn = os.path.basename(opath)
                        up_files[fn] = opath
                    
        upload_title = prompt_html('Uploading <style bg="yellow" fg="black">{} files...</style>'.format(len(up_files)))
        with ProgressBar(title=upload_title) as pb:
            l = list(up_files.items())
            # import random
            # random.shuffle(l)  # debug code for upload-hangs
            for fn,opath in pb(l):
                pb.title = prompt_html('Uploading <style bg="yellow" fg="black">{} files... ({})</style>'.format(len(up_files),fn))
                if fn.lower().endswith('.raw.zip'):
                    xfn = os.path.splitext(fn)[0]
                    with zipfile.ZipFile(opath, 'r') as rawzip:
                        tb = io.BytesIO(rawzip.read(xfn))
                        ftp.storbinary('STOR {}'.format(xfn), tb)
                else:
                    with open(opath,'rb') as fb:                  # file to send
                        ftp.storbinary('STOR {}'.format(fn), fb)
                        # TODO catch and process the server response
            pb.title = prompt_html('Uploading <style bg="yellow" fg="black">{} files... ({})</style>'.format(len(up_files),"fin."))
                


    click.echo("Done. Thank you for choosing BulkPRIDESubmission. Have a great day!")


if __name__ == "__main__":
    start()
