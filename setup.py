from setuptools import setup, find_packages

with open("README.md", "r") as fh:
    long_description = fh.read()
    
setup(
    name='BulkPrideSubmission',
    version='0.0.3',
    packages=find_packages(),
    url='https://gitlab.ebi.ac.uk/walzer/BulkPrideSubmission',
    description='Simple commandline tool to help with the manual bulk submissions to PRIDE.',
    long_description=long_description,
    install_requires=[
        "Click",
        "pronto",
        "prompt-toolkit"
    ],
    package_data={
        # If any package contains *.gz files, include them:
        "cv_library": ["*.gz"],
    },
    entry_points={
        'console_scripts': ['BulkPRIDESubmission = cli.bulk_pride_submission:start']
      }
)
