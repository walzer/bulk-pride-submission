# Simple command-line tool to help with the manual bulk submissions to PRIDE

```
Usage: BulkPRIDESubmission [OPTIONS] <target submission px file name>

Options:
  --prepared PATH  The absolute path of the file with prepared submission
                   data.

  --folder PATH    The absolute path of the folder(s) to submit. Inside each,
                   there should be the files to be submitted directly, no
                   subfolders.  [required]

  --help           Show this message and exit.
```

The `--folder` option can be repeated if there are multiple folders containing submission data. The folders are supposed to be 'flat', subfolders and hidden files ('.' prefix) are ignored.
The `--prepared` option can be used to reuse prepared submission data to avoid having to restate details of the submission. To prepare a submission, simply give a new file as option argument and state the details of the submission, which will be stored in the new file, which in turn is then ready for reuse a later time.
Files will be matched name first, up to the first period. Only matches with a `.raw` (or `.raw.zip`), a `.mzML`, and a `.mzid` (or `.mzid.gz`) file will be considered. If other file types match too, they will be included in the submission as well.


## During px generation
You will be requested to have two text files named `data_processing_protocol.md` and `sample_processing_protocol.md` ready in the current working directory (the one you executed the BulkPRIDESubmission helper from).
Autocompletion is case sensitive. Start typing and completion suggestions will appear. Intuitively, select with up/down and enter. After deleting characters, retype at least one character to have suggestions reappear.


### Container builds [![Docker Repository on Quay](https://quay.io/repository/mwalzer/bulk-pride-submission/status "Docker Repository on Quay")](https://quay.io/repository/mwalzer/bulk-pride-submission)

Build files for singularity and docker are in`container/`. 
For singularity:
* `singularity build /tmp/bulk-pride-submission.simg container/bulk-pride-submission.sdef`
For docker:
* `docker build -t bulkpridesubmission -f container/Dockerfile-alpine .`